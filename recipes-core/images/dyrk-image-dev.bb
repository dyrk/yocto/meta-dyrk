SUMMARY = "dyrk development image"

require dyrk-image.bb

IMAGE_FEATURES += "\
    debug-tweaks \
    ssh-server-openssh \
    tools-debug \
    "

IMAGE_INSTALL_append += "\
    ethtool \
    evtest \
    fbset \
    i2c-tools \
    memtester \
    screen \
    "

# vs-code-remote-dependencies
IMAGE_INSTALL_append += "\
    coreutils \
    curl \
    gcc \
    glibc \
    ldd \
    libstdc++ \
    nodejs \
    wget \
    "