SUMMARY = "dyrk production image"

IMAGE_FEATURES += "splash"

LICENSE = "MIT"

inherit core-image

VIRTUAL-RUNTIME_init_manager = "systemd"

IMAGE_INSTALL_append = "\
    systemd \
    python3 \
    python3-pip \
    python3-bme680 \
    python3-paho-mqqt \
    python3-smbus \
    bash \
    docker-ce \
    vim \
    nano \
    rpi-gpio \
    wpa-supplicant \
    linux-firmware-bcm43430 \
    bluez5 \
"

DISTRO_FEATURES_append ="\
    virtualization \
    systemd \
    "

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE_append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"
