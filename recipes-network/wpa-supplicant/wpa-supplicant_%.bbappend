do_install_append() {

	echo "network={
        ssid=\""${wpa_ssid}"\"
        psk=\""${wpa_pwd}"\"
        proto=RSN
        key_mgmt=WPA-PSK
}" >> ${D}${sysconfdir}/wpa_supplicant.conf;

}


SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN}_append = " wpa_supplicant@wlan0.service  "