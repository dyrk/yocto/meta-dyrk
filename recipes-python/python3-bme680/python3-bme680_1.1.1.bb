SUMMARY = "Python library for the BME680 temperature, humidity and gas sensor"
HOMEPAGE = "pimoroni.com"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=f1e0f2765d0deb24be669d1789b957d7"

SRC_URI[md5sum] = "494fa245a96efc09e49078194147c47e"
SRC_URI[sha256sum] = "287f9329d6befb88e091186502df87949ed796d5615419aa0ce92920752c852f"

PYPI_PACKAGE = "bme680"

inherit pypi setuptools3

RDEPENDS_${PN} += " \
    python3-psutil \
"