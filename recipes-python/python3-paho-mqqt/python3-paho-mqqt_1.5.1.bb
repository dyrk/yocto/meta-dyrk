
SUMMARY = "This document describes the source code for the \
Eclipse Paho MQTT Python client library, which implements versions \
 5.0, 3.1.1, and 3.1 of the MQTT protocol."
HOMEPAGE = "https://www.eclipse.org/paho/"
LICENSE = "EPL-1.0"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=eb48c6ee2cb9f5b8b9fe75e6f817bdfc"

SRC_URI[md5sum] = "32f93c0ed92c7439f7a715ed258fd35d"
SRC_URI[sha256sum] = "9feb068e822be7b3a116324e01fb6028eb1d66412bf98595ae72698965cb1cae"

PYPI_PACKAGE = "paho-mqtt"

inherit pypi setuptools3

RDEPENDS_${PN} += " \
    python3-psutil \
"